name := "StringTree"

ThisBuild/organization := "org.bitbucket.nhweston"
ThisBuild/scalaVersion := "2.12.8"
ThisBuild/scalacOptions :=
    Seq(
        "-deprecation",
        "-feature",
        "-unchecked",
        "-Xcheckinit",
    )

lazy val stringtree = (project in file(".")).settings(
    name := "StringTree",
    version := "0.1.0",
    libraryDependencies ++= Seq(
        "org.scalatest" %% "scalatest" % "3.0.5" % "test",
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.2.0" withSources(),
        "junit" % "junit" % "4.12" % Test
    )
)
.settings(
    test in assembly := {},
    assemblyJarName in assembly := "jstringtree_raw.jar",
)
