/*
 * StringTree
 * Copyright (C) 2018 Nicholas Weston
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

package org.bitbucket.nhweston.stringtree;

import scala.Option;
import scala.Tuple2;
import scala.collection.JavaConverters;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * A recursive data structure consisting of string keys each mapped to either a string or a map.
 *
 * @author Nicholas Weston
 */
public class JStringTree {

    private final Node.Branch node;

    /**
     * Creates a new empty string tree.
     */
    public JStringTree() {
        node = new Node.Branch();
    }

    JStringTree(Node.Branch node) {
        this.node = node;
    }

    /**
     * Inserts a new empty string tree and returns it. The associated key is the string representation of the lowest
     * non-negative integer (in decimal) not currently used as a key.
     *
     * @return  the new string tree.
     */
    public JStringTree addBranch() {
        return new JStringTree(node.branch());
    }

    /**
     * Attempts to ensure that the given key is associated with a string tree.
     *  <ul>
     *      <li>If the key is not present, the key is associated with a new empty string tree, which is subsequently
     *      returned.</li>
     *      <li>If the key is already associated with a string tree, the map is left unchanged and the existing string
     *      tree is returned.</li>
     *      <li>If the key is already associated with a string, the map is left unchanged and <code>null</code> is
     *      returned.</li>
     *  </ul>
     *
     * @param key   the key to associate.
     * @return      the string tree associated with <code>null</code> following this operation, or <code>null</code> if
     *              no such string tree exists.
     */
    public JStringTree putBranch(String key) {
        Option<Node.Branch> result = node.branch(key);
        if (result.isDefined()) {
            return new JStringTree(result.get());
        }
        return null;
    }

    /**
     * Inserts the specified string into this string tree. The associated key is the string representation of the
     * lowest non-negative integer (in decimal) not currently used as a key.
     *
     * @param value     the string to insert.
     */
    public void addLeaf(String value) {
        node.leaf(value);
    }

    /**
     * Attempts to associated the given key to the given string. If the key is already present, the map is left
     * unchanged.
     *
     * @param key       the key to associate to the string.
     * @param value     the string to insert.
     * @return          <code>true</code> if this operation resulted in a change in the map, <code>false</code>
     *                  otherwise.
     */
    public boolean putLeaf(String key, String value) {
        return node.leaf(key, value).isDefined();
    }

    /**
     * Returns the string tree with the given key.
     *
     * @param key   the key of the desired string tree.
     * @return      the string tree associated with <code>key</code>, or <code>null</code> if no such string tree
     *              exists.
     */
    public JStringTree getBranch(String key) {
        Option<Node.Branch> result = node.getBranch(key);
        if (result.isDefined()) {
            return new JStringTree(result.get());
        }
        return null;
    }

    /**
     * Returns the string with the given key.
     *
     * @param key   the key of the desired string.
     * @return      the string associated with <code>key</code>, or <code>null</code> if no such string exists.
     */
    public String getLeaf(String key) {
        Option<String> result = node.getLeaf(key);
        if (result.isDefined()) {
            return result.get();
        }
        return null;
    }

    /**
     * Checks whether this string tree contains a mapping for the given key.
     *
     * @param key   the key to check.
     * @return      <code>true</code> if <code>key</code> has a mapping, <code>false</code> otherwise.
     */
    public boolean containsAny(String key) {
        return node.containsAny(key);
    }

    /**
     * Checks whether the given key is mapped to a string tree.
     *
     * @param key   the key to check.
     * @return      <code>true</code> if <code>key</code> is mapped to a string tree, <code>false</code> otherwise.
     */
    public boolean containsBranch(String key) {
        return node.containsBranch(key);
    }

    /**
     * Checks whether the given key is mapped to a string.
     *
     * @param key   the key to check.
     * @return      <code>true</code> if <code>key</code> is mapped to a string, <code>false</code> otherwise.
     */
    public boolean containsLeaf(String key) {
        return node.containsLeaf(key);
    }

    /**
     * Attempts to return the string tree at the given path.
     *
     * @param keys  the path of the desired string tree, represented as a list of keys.
     * @return      the string tree at the given path, or <code>null</code> if no such string tree exists.
     */
    public JStringTree nav(List<String> keys) {
        Option<Node.Branch> result = node.nav(JavaConverters.asScalaBuffer(keys).toList());
        if (result.isDefined()) {
            return new JStringTree(result.get());
        }
        return null;
    }

    /**
     * Attempts to return the string tree at the given path.
     *
     * @param path  the path of the desired string tree, represented as a string of keys delimited by periods
     *              (<code>'.'</code>).
     * @return      the string tree at the given path, or <code>null</code> if no such string tree exists.
     */
    public JStringTree nav(String path) {
        return nav(Arrays.asList(path.split("\\.", -1)));
    }

    /**
     * Returns a map containing all entries in this string tree whose values are string trees.
     *
     * @return  a map of branch entries in this string tree.
     */
    public Map<String, JStringTree> branches() {
        Map<String, JStringTree> result = new LinkedHashMap<>();
        List<Tuple2<String, Node.Branch>> resultScala = JavaConverters.seqAsJavaList(node.branchSeq());
        for (Tuple2<String, Node.Branch> entry : resultScala) {
            result.put(entry._1, new JStringTree(entry._2));
        }
        return result;
    }

    /**
     * Returns a map containing alll entries in this string tree whose values are strings.
     *
     * @return  a map of leaf entries in this string tree.
     */
    public Map<String, String> leaves() {
        Map<String, String> result = new LinkedHashMap<>();
        List<Tuple2<String, String>> resultScala = JavaConverters.seqAsJavaList(node.leafSeq());
        for (Tuple2<String, String> entry : resultScala) {
            result.put(entry._1, entry._2);
        }
        return result;
    }

    /**
     * Returns a map containing all strings reachable from this string tree with paths represented as keys delimited by
     * periods (<code>'.'</code>).
     *
     * @return  a flat map representation of this string tree.
     */
    public Map<String, String> toMap() {
        return JavaConverters.mapAsJavaMap(node.flatMap());
    }

    @Override
    public String toString() {
        return node.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JStringTree) {
            JStringTree other = (JStringTree) obj;
            return (this.node.equals(other.node));
        }
        return false;
    }

}
