/*
 * StringTree
 * Copyright (C) 2018 Nicholas Weston
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

package org.bitbucket.nhweston.stringtree;

import scala.collection.JavaConverters;

import java.util.List;

/**
 * Builds a {@link StringTree} from string tree files.
 *
 * @author Nicholas Weston
 */
public class JStringTreeParser {

    private final Parser parser;

    /**
     * Creates a new string tree parser with an empty string tree as its target.
     */
    public JStringTreeParser() {
        parser = new Parser();
    }

    /**
     * Loads the given string tree file into this string tree.
     *
     * @param filePath  the string tree file to load.
     * @return          a sequence of output messages.
     */
    public List<String> parse(String filePath) {
        return JavaConverters.seqAsJavaList(parser.parse(filePath));
    }

    /**
     * Returns the string tree built by this parser.
     *
     * @return  the string tree built by this parser.
     */
    public JStringTree get() {
        return new JStringTree(parser.root());
    }

}
