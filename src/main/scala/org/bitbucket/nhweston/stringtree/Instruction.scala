/*
 * StringTree
 * Copyright (C) 2018 Nicholas Weston
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

package org.bitbucket.nhweston.stringtree

import org.bitbucket.inkytonik.kiama.util.Message
import org.bitbucket.nhweston.stringtree.Node.{Branch, Leaf}

sealed abstract class Instruction

object Instruction {

    sealed abstract class Buildable extends Instruction {
        def build (node: Branch) : Vector[Message]
    }

    type Entry = (Option[String], Instruction)

    case class IBranch (entries: Vector[Entry]) extends Buildable {
        override def build (node: Branch) : Vector[Message] = {
            var msgs: Vector[Message] = Vector()
            for (entry <- entries) entry match {
                case (None, child: Buildable) => msgs ++= child.build(node.branch())
                case (None, ILeaf(value)) => node.leaf(value)
                case (Some(key: String), child: Buildable) => node.branch(key) match {
                    case Some(existing) => msgs ++= child.build(existing)
                    case None => msgs :+= Message(entry, s"Attempt to overwrite `$key`")
                }
                case (Some(key: String), ILeaf(value)) => node.leaf(key, value) match {
                    case Some(_) => ()
                    case None => msgs :+= Message(entry, s"Attempt to overwite `$key`")
                }
                case _ => msgs :+= Message(entry, "Invalid entry")
            }
            msgs
        }
    }

    case class ITable (keys: Vector[String], rows: Vector[IRow]) extends Buildable {
        override def build (node: Branch) : Vector[Message] = {
            var msgs: Vector[Message] = Vector()
            var colSet: Set[String] = Set()
            for (key <- keys) {
                if (colSet.contains(key)) {
                    return Vector(Message(this, s"Table contains duplicate key `$key`"))
                }
                colSet += key
            }
            for (row <- rows) {
                msgs ++= row.build(node.branch(), keys)
            }
            msgs
        }
    }

    case class IRow (leaves: Vector[ILeaf]) extends Instruction {
        def build (node: Branch, keys: Vector[String]) : Option[Message] = {
            if (leaves.size != keys.size) {
                Some(Message(this, s"Table with ${keys.size} columns contains row with ${leaves.size} columns"))
            }
            else {
                keys.zip(leaves).foreach {
                    case (key, ILeaf(value)) => node.get(key) match {
                        case None => node.put(key, Leaf(value))
                        case Some(_) => return Some(Message(this, s"Attempt to overwrite `$key`"))
                    }
                }
                None
            }
        }
    }

    case class ILeaf (value: String) extends Instruction

    case class IText (value: Seq[String]) extends Instruction

}