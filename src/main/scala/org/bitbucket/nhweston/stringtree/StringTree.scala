/*
 * StringTree
 * Copyright (C) 2018 Nicholas Weston
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

package org.bitbucket.nhweston.stringtree

/**
 * A recursive data structure consisting of string keys each mapped to either a string or a map. This trait provides a
 * read-only interface to [[Node.Branch]].
 */
trait StringTree {

    /**
     * Returns the string tree with the given key.
     *
     * @param key   the key of the desired string tree.
     * @return      `Some` of the string tree associated with `key`, or `None` if no such string tree exists.
     */
    def getBranch (key: String) : Option[StringTree]

    /**
     * Returns the string with the given key.
     *
     * @param key   the key of the desired string.
     * @return      `Some` of the string associated with `key`, or `None` if no such string exists.
     */
    def getLeaf (key: String) : Option[String]

    /**
     * Checks whether this string tree contains a mapping for the given key.
     *
     * @param key   the key to check.
     * @return      `true` if `key` has a mapping, `false` otherwise.
     */
    def containsAny (key: String) : Boolean

    /**
     * Checks whether the given key is mapped to a string tree.
     *
     * @param key   the key to check.
     * @return      `true` if `key` is mapped to a string tree, `false` otherwise.
     */
    def containsBranch (key: String) : Boolean

    /**
     * Checks whether the given key is mapped to a string.
     *
     * @param key   the key to check.
     * @return      `true` if `key` is mapped to a string, `false` otherwise.
     */
    def containsLeaf (key: String) : Boolean

    /**
     * Attempts to return the string tree at the given path.
     *
     * @param keys  the path of the desired string tree, represented as a list of keys.
     * @return      `Some` of the string tree at the given path, or `None` if no such string tree exists.
     */
    def nav (keys: Seq[String]) : Option[StringTree]

    /**
     * Attempts to return the string tree at the given path.
     *
     * @param path  the path of the desired string tree, represented as a string of keys delimited by periods (`'.'`).
     * @return      `Some` of the string tree at the given path, or `None` if no such string tree exists.
     */
    def nav (path: String) : Option[StringTree]

    /**
     * Returns a sequence of entries in this string tree. The values are represented as an `Either` of `String` and
     * `StringTree`.
     *
     * @return  a sequence of entries in this string tree.
     */
    def entrySeq : Seq[(String, Either[String, StringTree])]

    /**
     * Returns a sequence of entries in this string tree whose values are string trees.
     *
     * @return  a sequence of branch entries in this string tree.
     */
    def branchSeq : Seq[(String, StringTree)]

    /**
     * Returns a sequence of entries in the string tree whose values are strings.
     *
     * @return  a sequence of leaf entries in this string tree.
     */
    def leafSeq : Seq[(String, String)]

    /**
     * Returns a sequence of all strings reachable from this string tree with their paths represented as keys delimited
     * by periods (`'.'`).
     *
     * @return  a flat entry list representation of this string tree.
     */
    def flatSeq : Seq[(String, String)]

    /**
     * Returns a map containing all strings reachable from this string tree with paths represented as keys delimited by
     * periods (`'.'`).
     *
     * @return  a flat map representation of this string tree.
     */
    def flatMap : Map[String, String]

}