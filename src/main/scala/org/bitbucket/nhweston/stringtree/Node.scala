/*
 * StringTree
 * Copyright (C) 2018 Nicholas Weston
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

package org.bitbucket.nhweston.stringtree

import scala.collection.mutable

/**
 * A string tree node, either a `Branch` or a `Leaf`.
 */
sealed trait Node

object Node {

    /**
     * A string tree node containing mappings. In other words, a string tree. This class should be accessed via the
     * `StringTree` trait for read-only use cases.
     */
    final case class Branch () extends Node with StringTree {

        private lazy val entries: mutable.LinkedHashMap[String, Node] = new mutable.LinkedHashMap()

        private var serial: Int = 0

        /**
         * Inserts the specified node into this string tree. The associated key is the string representation of the
         * lowest non-negative integer (in decimal) not currently used as a key.
         *
         * @param node  the node to insert.
         */
        def add (node: Node) : Unit = {
            val key = serial.toString
            entries.get(key) match {
                case None => entries.put(key, node)
                case Some(_) =>
                    serial += 1
                    add(node)
            }
        }

        /**
         * Associates the specified key to the specified node. This method should be used with caution as it is the
         * only method capable of overwriting existing mappings.
         *
         * @param key   the key to associate to the node.
         * @param node  the node to insert.
         */
        def put (key: String, node: Node) : Unit = {
            entries.put(key, node)
        }

        /**
         * Inserts a new empty string tree and returns it. The associated key is the string representation of the
         * lowest non-negative integer (in decimal) not currently used as a key.
         *
         * @return  the new string tree.
         */
        def branch () : Branch = {
            val node: Branch = Branch()
            add(node)
            node
        }

        /**
         * Attempts to ensure that the given key is associated with a string tree.
         *  <ul>
         *      <li>If the key is not present, the key is associated with a new empty string tree, which is
         *      subsequently returned in a `Some`.</li>
         *      <li>If the key is already associated with a string tree, the map is left unchanged and the existing
         *      string tree is returned in a `Some`.</li>
         *      <li>If the key is already associated with a string, the map is left unchanged and `None` is
         *      returned.</li>
         *  </ul>
         *
         * @param key   the key to associate.
         * @return      `Some` of the string tree associated with `key` following this operation, or `None` if no such
         *              string tree exists.
         */
        def branch (key: String) : Option[Branch] = entries.get(key) match {
            case Some(existing @ Branch()) => Some(existing)
            case None =>
                val node: Branch = Branch()
                entries.put(key, node)
                Some(node)
            case _ => None
        }

        /**
         * Inserts the specified string into this string tree. The associated key is the string representation of the
         * lowest non-negative integer (in decimal) not currently used as a key.
         *
         * @param value     the string to insert.
         * @return          the node inserted by this operation.
         */
        def leaf (value: String) : Leaf = {
            val node: Leaf = Leaf(value)
            add(node)
            node
        }

        /**
         * Attempts to associated the given key to the given string and returns it in a `Some`. If the key is already
         * present, the map is left unchanged and `None` is returned.
         *
         * @param key       the key to associate to the string.
         * @param value     the string to insert.
         * @return          `Some` of the leaf node associated with `key` following this operation, or `None` if no
         *                  such leaf node exists.
         */
        def leaf (key: String, value: String) : Option[Leaf] = entries.get(key) match {
            case None =>
                val node: Leaf = Leaf(value)
                entries.put(key, node)
                Some(node)
            case _ => None
        }

        /**
         * Returns the node with the specified key.
         *
         * @param key   the key of the desired node.
         * @return      `Some` of the node with the given key, or `None` if the key is not present.
         */
        def get (key: String) : Option[Node] = entries.get(key)

        override def getBranch (key: String) : Option[Branch] = get(key) match {
            case Some (branch @ Branch()) => Some(branch)
            case _ => None
        }

        override def getLeaf (key: String) : Option[String] = get(key) match {
            case Some (Leaf(value)) => Some(value)
            case _ => None
        }

        override def containsAny (key: String) : Boolean = get(key) match {
            case Some(_) => true
            case _ => false
        }

        override def containsBranch (key: String) : Boolean = get(key) match {
            case Some (Branch()) => true
            case _ => false
        }

        override def containsLeaf (key: String) : Boolean = get(key) match {
            case Some (Leaf(_)) => true
            case _ => false
        }

        override def nav (keys: Seq[String]) : Option[Branch] = keys match {
            case Nil => Some(this)
            case key :: rem => get(key) match {
                case Some (next @ Branch()) => next.nav(rem)
                case _ => None
            }
        }

        override def nav (path: String) : Option[StringTree] = {
            nav(path.split("\\.", -1).toList)
        }

        /**
         * Returns a sequence of entries in this string tree as string-node pairs.
         *
         * @return  a sequence of entries in this string tree.
         */
        def nodeSeq : Seq[(String, Node)] = entries.toSeq

        override def entrySeq : Seq[(String, Either[String, StringTree])] = {
            nodeSeq.map {
                case (key, branch @ Branch()) => (key, Right(branch))
                case (key, Leaf(value)) => (key, Left(value))
            }
        }

        override def branchSeq : Seq[(String, Branch)] = nodeSeq.collect {
            case (key, branch @ Branch()) => (key, branch)
        }

        override def leafSeq : Seq[(String, String)] = nodeSeq.collect {
            case (key, Leaf(value)) => (key, value)
        }

        override def flatSeq : Seq[(String, String)] = flatSeq("")

        def flatSeq (prefix: String) : Seq[(String, String)] = nodeSeq.foldLeft(Seq[(String, String)]()) {
            case (seq, (key, branch @ Branch())) => seq ++ branch.flatSeq(s"$prefix$key.")
            case (seq, (key, Leaf(value))) => seq :+ ((s"$prefix$key", value))
        }

        override def flatMap : Map[String, String] = {
            flatSeq.toMap
        }

        override def toString : String = {
            toString(0)
        }

        def toString (tab: Int) : String = {
            entries.foldLeft("") { case (string, (key, node)) =>
                val isSerial = key.matches("""[0-9]+""")
                val indent = " " * tab
                val lhs = if (isSerial) "#" else key
                val rhs = node match {
                    case child @ Branch() => s" {\n${child.toString(tab + 2)}" + indent + "}"
                    case Leaf(value) =>
                        val valueAsString: String = {
                            if (value.contains("\n")) {
                                val quotes: String = "\n" + indent + "\"\"\""
                                quotes + "\n" + indent + value.replaceAll("\n", "\n" + indent) + quotes
                            }
                            else if (value.matches(Syntax.REGEX_NUMERIC) || value.matches(Syntax.REGEX_LITERAL)) value
                            else "\"" + value + "\""
                        }
                        (if (isSerial) " " else ": ") + s"""$valueAsString"""
                }
                string + indent + s"$lhs$rhs\n"
            }
        }

    }

    /**
     * A leaf node of a string tree containing a string.
     *
     * @param value     the string contained by this node.
     */
    case class Leaf (value: String) extends Node

}