/*
 * StringTree
 * Copyright (C) 2018 Nicholas Weston
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

package org.bitbucket.nhweston.stringtree

import org.bitbucket.inkytonik.kiama.parsing.{NoSuccess, Success}
import org.bitbucket.inkytonik.kiama.util.{FileSource, Message, Messaging, PositionStore}
import org.bitbucket.nhweston.stringtree.Node.Branch

/**
 * Builds a string tree from string tree files.
 */
class Parser extends PositionStore with Messaging {

    /**
     * The string tree built by this parser.
     */
    val root: Branch = Branch()

    /**
     * Loads the given string tree file into this string tree.
     *
     * @param filePath  the string tree file to load.
     * @return          a sequence of output messages.
     */
    def parse (filePath: String) : Seq[String] = {
        val source: FileSource = FileSource(filePath)
        val parsers: Syntax = new Syntax(positions)
        parsers.parse(parsers.file, source) match {
            case Success(instr, _) => instr.build(root).foldLeft(Seq[String]())(_ :+ formatMessage(_))
            case error: NoSuccess =>
                positions.setAllPositions(error, error.next.position)
                Seq(formatMessage(Message(error, error.message)))
        }
    }

}