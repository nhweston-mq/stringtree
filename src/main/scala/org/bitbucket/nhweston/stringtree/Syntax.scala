/*
 * StringTree
 * Copyright (C) 2018 Nicholas Weston
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

package org.bitbucket.nhweston.stringtree

import org.bitbucket.inkytonik.kiama.parsing.Parsers
import org.bitbucket.inkytonik.kiama.util.Positions
import org.bitbucket.nhweston.stringtree.Instruction._
import org.bitbucket.nhweston.stringtree.Syntax._

class Syntax (positions: Positions) extends Parsers (positions) {

    lazy val file: PackratParser[IBranch] = {
        phrase(map) |
        failure("String tree not found")
    }

    lazy val branch: PackratParser[Instruction] = {
        "{" ~> (map | table | failure("Branch expected")) <~ "}" |
        "{" ~ "}" ^^^ IBranch(Vector()) |
        failure("Branch expected")
    }

    lazy val leaf: PackratParser[ILeaf] = {
        textblock | (literal | quotation | numeric | failure("Value expected")) ^^ ILeaf
    }

    lazy val map: PackratParser[IBranch] = {
        rep1(entry) ^^ IBranch |
        failure("Map expected")
    }

    lazy val table: PackratParser[ITable] = {
        ("$" ~> rep(literal) <~ "|") ~ repsep(row, "|") ^^ ITable |
        failure("Table expected")
    }

    lazy val row: PackratParser[IRow] = {
        rep(leaf) ^^ IRow |
        failure("Row expected")
    }

    lazy val entry: PackratParser[Entry] = {
        serial | path | failure("Entry expected")
    }

    lazy val serial: PackratParser[Entry] = {
        "#" ~> (leaf | branch | failure("Node expected")) ^^ {(None, _)} |
        failure("Serial expected")
    }

    lazy val path: PackratParser[Entry] = {
        literal ~ (":" ~> leaf | branch | failure("Node expected")) ^^ {case k ~ v => (Some(k), v)} |
        literal ~ ("." ~> path | serial | failure("Path expected")) ^^ {case k ~ e => (Some(k), IBranch(Vector(e)))} |
        failure("Path expected")
    }

    lazy val literal: PackratParser[String] = {
        regex(REGEX_LITERAL.r) |
        failure("Literal expected")
    }

    lazy val quotation: PackratParser[String] = {
        "\"" ~> regex("""[^"]*""".r) <~ "\"" |
        failure("Quotation expected")
    }

    lazy val textblock: PackratParser[ILeaf] = {
        "\"\"\"" ~ textline ^^ {
            case quotes ~ lines =>
                val start: Int = positions.getStart(quotes).get.column
                def create (list: Seq[String]) : String = list match {
                    case head :: rem =>
                        val offset: Int = positions.getStart(head).get.column - start
                        val line: String = " " * offset + head
                        rem match {
                            case Nil => line
                            case _ => line + "\n" + create(rem)
                        }
                }
                ILeaf(create(lines))
        }
    }

    lazy val textline: PackratParser[Seq[String]] = {
        regex("\"\"\"".r) ^^^ Nil |
        regex("((?!\"\"\").)*".r) ~ textline ^^ {case line ~ rem => line +: rem}
    }

    lazy val numeric: PackratParser[String] = {
        regex(REGEX_NUMERIC.r) |
        failure("Numeric expected")
    }

    override val whitespace: Parser[String] = {
        regex("""(\s|(//.*(\R|\z)))*""".r)
    }

}

object Syntax {

    val REGEX_LITERAL: String = """[a-zA-Z_][a-zA-Z0-9_]*"""
    val REGEX_NUMERIC: String = """-?[0-9]+(\.[0-9]+)?"""

}