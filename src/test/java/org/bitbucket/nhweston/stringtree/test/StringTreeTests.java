/*
 * StringTree
 * Copyright (C) 2018 Nicholas Weston
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

package org.bitbucket.nhweston.stringtree.test;

import org.bitbucket.nhweston.stringtree.JStringTree;
import org.junit.Assert;
import org.junit.Test;

public class StringTreeTests {

    @Test
    public void empty_contains() {
        JStringTree str = new JStringTree();
        Assert.assertFalse(str.containsBranch("foo"));
        Assert.assertFalse(str.containsLeaf("bar"));
        Assert.assertFalse(str.containsAny("baz"));
    }

    @Test
    public void empty_get() {
        JStringTree str = new JStringTree();
        Assert.assertNull(str.getBranch("qwerty"));
        Assert.assertNull(str.getLeaf("uiop"));
    }

    @Test
    public void empty_nav() {
        JStringTree str = new JStringTree();
        Assert.assertNull(str.nav("a"));
        Assert.assertNull(str.nav("a.b"));
        Assert.assertNull(str.nav("..."));
        Assert.assertNull(str.nav(" "));
    }

    @Test
    public void empty_map() {
        JStringTree str = new JStringTree();
        Assert.assertTrue(str.branches().isEmpty());
        Assert.assertTrue(str.leaves().isEmpty());
        Assert.assertTrue(str.toMap().isEmpty());
    }

    @Test
    public void add_one_branch() {
        JStringTree str = new JStringTree();
        JStringTree branch = str.addBranch();
        Assert.assertNotNull(branch);
        Assert.assertEquals(branch, str.getBranch("0"));
    }

    @Test
    public void add_one_leaf() {
        JStringTree str = new JStringTree();
        str.addLeaf("qwerty");
        Assert.assertEquals("qwerty", str.getLeaf("0"));
    }

    @Test
    public void put_one_branch() {
        JStringTree str = new JStringTree();
        JStringTree branch = str.putBranch("asdf");
        Assert.assertNotNull(branch);
        Assert.assertEquals(branch, str.getBranch("asdf"));
    }

    @Test
    public void put_one_leaf() {
        JStringTree str = new JStringTree();
        Assert.assertTrue(str.putLeaf("zxcv", "bnm"));
        Assert.assertEquals("bnm", str.getLeaf("zxcv"));
    }

    @Test
    public void add_and_put_many() {
        JStringTree str = new JStringTree();
        JStringTree br0 = str.addBranch();
        Assert.assertTrue(str.putLeaf("foo", "qwerty"));
        str.addLeaf("uiop");
        JStringTree brBar = str.putBranch("bar");
        Assert.assertTrue(str.putLeaf("baz", "asdf"));
        JStringTree br2 = str.addBranch();
        JStringTree brQux = str.putBranch("qux");
        str.addLeaf("ghjkl");
        Assert.assertEquals(br0, str.getBranch("0"));
        Assert.assertEquals("qwerty", str.getLeaf("foo"));
        Assert.assertEquals("uiop", str.getLeaf("1"));
        Assert.assertEquals(brBar, str.getBranch("bar"));
        Assert.assertEquals("asdf", str.getLeaf("baz"));
        Assert.assertEquals(br2, str.getBranch("2"));
        Assert.assertEquals(brQux, str.getBranch("qux"));
        Assert.assertEquals("ghjkl", str.getLeaf("3"));
    }

}
